﻿using ManejoIncidente.Datos.Incidentes;
using ManejoIncidente.Logica.HistorialIncidentes;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.Incidentes
{
    public class IncidenteLogica
    {
        private readonly IncidenteContext _incidenteContext;
        private readonly HistorialLogica _historialLogica;
        public IncidenteLogica()
        {
            _incidenteContext = new IncidenteContext();
            _historialLogica = new HistorialLogica();
        }

        public List<Incidente> ConsultarIncidente(int id, string tipo = "")
        {
            var incidentes = _incidenteContext.ConsultarIncidentes(id, tipo);
            foreach (var item in incidentes)
            {
                switch (item.Estatus)
                {
                    case "AS":
                        item.Estatus = "ASIGNADO";
                        break;
                    case "CE":
                        item.Estatus = "Cerrado";
                        break;
                    case "PR":
                        item.Estatus = "Progreso";
                        break;
                    case "RE":
                        item.Estatus = "Rechazado";
                        break;
                    case "CR":
                        item.Estatus = "Creado";
                        break;
                }
            }
            return incidentes;
        }

        public void InsertarIncidente(Incidente incidente)
        {
            incidente.ComentarioCierre = string.IsNullOrEmpty(incidente.ComentarioCierre) ? string.Empty : incidente.ComentarioCierre;
            incidente.Descripcion = string.IsNullOrEmpty(incidente.Descripcion) ? string.Empty : incidente.Descripcion;
            incidente.Titulo = string.IsNullOrEmpty(incidente.Titulo) ? string.Empty : incidente.Titulo;
            _incidenteContext.InsertarIncidente(incidente);
        }

        public void ActualizarIncidente(Incidente incidente)
        {
            incidente.ComentarioCierre = string.IsNullOrEmpty(incidente.ComentarioCierre) ? string.Empty : incidente.ComentarioCierre;
            incidente.Descripcion = string.IsNullOrEmpty(incidente.Descripcion) ? string.Empty : incidente.Descripcion;
            incidente.Titulo = string.IsNullOrEmpty(incidente.Titulo) ? string.Empty : incidente.Titulo;
            var cambios = new List<string>();
            var incidenteValidacion = ConsultarIncidente(incidente.IncidenteId)[0];
            if (!incidenteValidacion.Titulo.Equals(incidente.Titulo)) cambios.Add($"Titulo: {incidenteValidacion.Titulo}>>>{incidente.Titulo}");
            if (!incidenteValidacion.Descripcion.Equals(incidente.Descripcion)) cambios.Add($"Descripcion {incidenteValidacion.Descripcion}>>>{incidente.Descripcion}");
            if (!incidenteValidacion.DepartamentoId.Equals(incidente.DepartamentoId)) cambios.Add($"Departamento {incidenteValidacion.DepartamentoId}>>>{incidente.DepartamentoId}");
            if (!incidenteValidacion.PrioridadId.Equals(incidente.PrioridadId)) cambios.Add($"Prioridad {incidenteValidacion.PrioridadId}>>>{incidente.PrioridadId}");
            if (!incidenteValidacion.ComentarioCierre.Equals(incidente.ComentarioCierre)) cambios.Add($"Comentario {incidenteValidacion.ComentarioCierre}>>>{incidente.ComentarioCierre}");
            _incidenteContext.ActualizarIncidente(incidente);
            foreach (var item in cambios)
            {
                var historial = new HistorialIncidente()
                {
                    Comentario = item,
                    Estatus = "A",
                    IncidenteId = incidente.IncidenteId,
                    CreadoPor = incidente.ModificadoPor,
                    FechaIngreso = DateTimeOffset.Now
                };
                _historialLogica.InsertarHistorial(historial);
            };
        }

        public void Asignar(Incidente incidente)
        {
            incidente.ComentarioCierre = string.IsNullOrEmpty(incidente.ComentarioCierre) ? string.Empty : incidente.ComentarioCierre;
            var cambios = new List<string>();
            var incidenteValidacion = ConsultarIncidente(incidente.IncidenteId)[0]; 
            _incidenteContext.Asignar(incidente);
            cambios.Add($"Asignado a usuario {incidente.UsuarioAsignadoId} por el usuario {incidente.ModificadoPor}");
            if (!incidenteValidacion.ComentarioCierre.Equals(incidente.ComentarioCierre)) cambios.Add($"Comentario {incidenteValidacion.ComentarioCierre}>>>{incidente.ComentarioCierre}");
            foreach (var item in cambios)
            {
                var historial = new HistorialIncidente()
                {
                    Comentario = item,
                    Estatus = "A",
                    IncidenteId = incidente.IncidenteId,
                    CreadoPor = incidente.ModificadoPor,
                    FechaIngreso = DateTimeOffset.Now
                };
                _historialLogica.InsertarHistorial(historial);
            };

        }

        public void CambiarEstado(Incidente incidente)
        {
            incidente.ComentarioCierre = string.IsNullOrEmpty(incidente.ComentarioCierre) ? string.Empty : incidente.ComentarioCierre;

            var incidenteValidacion = ConsultarIncidente(incidente.IncidenteId)[0];
            var cambios = new List<string>();
            if (!incidenteValidacion.ComentarioCierre.Equals(incidente.ComentarioCierre)) cambios.Add($"Comentario {incidenteValidacion.ComentarioCierre}>>>{incidente.ComentarioCierre}");
            if (!incidenteValidacion.Estatus.Equals(incidente.ComentarioCierre)) cambios.Add($"Estatus {incidenteValidacion.Estatus}>>>{incidente.Estatus}");
            _incidenteContext.CambiarEstado(incidente);
            foreach (var item in cambios)
            {
                var historial = new HistorialIncidente()
                {
                    Comentario = item,
                    Estatus = "A",
                    IncidenteId = incidente.IncidenteId,
                    CreadoPor = incidente.ModificadoPor,
                    FechaIngreso = DateTimeOffset.Now
                };
                _historialLogica.InsertarHistorial(historial);
            };
        }

        public void EliminarIncidente(int id, int modificado)
        {
            _incidenteContext.EliminarIncidente(id);
            var historial = new HistorialIncidente()
            {
                Comentario = "Se elimino el incidente",
                Estatus = "A",
                IncidenteId = id,
                CreadoPor = modificado,
                FechaIngreso = DateTimeOffset.Now
            };
            new HistorialLogica().InsertarHistorial(historial);
        }

    }
}
