﻿using ManejoIncidente.Datos.Slas;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.Slas
{
    public class SlaLogica
    {
        private readonly SlaContext _slaContext;

        public SlaLogica()
        {
            _slaContext = new SlaContext();
        }

        public List<Sla> ConsultarSla(int id)
        {
            return _slaContext.ConsultarSla(id);
        }

        public void InsertarSla(Sla sla)
        {
            sla.Descripcion = string.IsNullOrEmpty(sla.Descripcion) ? string.Empty : sla.Descripcion;
            _slaContext.InsertarSla(sla);
        }

        public void ActualizarSla(Sla sla)
        {
            sla.Descripcion = string.IsNullOrEmpty(sla.Descripcion) ? string.Empty : sla.Descripcion;
            _slaContext.ActualizarSla(sla);
        }

        public void ElimiarSla(int id)
        {
            _slaContext.EliminarSla(id);
        }
    }
}
