﻿using ManejoIncidente.Datos.Departamentos;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.Departamentos
{
    public class DepartamentoLogica
    {
        private readonly DepartamentoContext _departamentoContext;

        public DepartamentoLogica()
        {
            _departamentoContext = new DepartamentoContext();
        }

        public List<Departamento> ConsultaDepartamento(int id)
        {
            return _departamentoContext.ConsultarDepartamentos(id);
        }

        public void InsertarDepartamento(Departamento departamento)
        {
            departamento.Nombre = string.IsNullOrEmpty(departamento.Nombre) ? string.Empty : departamento.Nombre;
            _departamentoContext.InsertarDepartamento(departamento);
        }

        public void ActualizarDepartamento(Departamento departamento)
        {
            departamento.Nombre = string.IsNullOrEmpty(departamento.Nombre) ? string.Empty : departamento.Nombre;
            _departamentoContext.ActualizarDepartamento(departamento);
        }

        public void EliminarDepartamento(int id)
        {
            _departamentoContext.EliminarDepartamento(id);
        }
    }
}
