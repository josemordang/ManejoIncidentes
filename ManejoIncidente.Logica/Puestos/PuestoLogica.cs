﻿using ManejoIncidente.Datos.Puestos;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.Puestos
{
    public class PuestoLogica
    {
        private readonly PuestoContext _puestoContext;
        public PuestoLogica()
        {
            _puestoContext = new PuestoContext();
        }

        public List<Puesto> ConsultarPuesto(int id)
        {
            return _puestoContext.ConsultarPuestos(id);
        }

        public void InsertarPuesto(Puesto puesto)
        {
            puesto.Nombre = string.IsNullOrEmpty(puesto.Nombre) ? string.Empty : puesto.Nombre;
            _puestoContext.InsertarPuesto(puesto);
        }

        public void ActualizarPuesto(Puesto puesto)
        {
            puesto.Nombre = string.IsNullOrEmpty(puesto.Nombre) ? string.Empty : puesto.Nombre;
            _puestoContext.ActualizarPuesto(puesto);
        }

        public void EliminarPuesto(int id)
        {
            _puestoContext.EliminarPuesto(id);
        }

    }
}
