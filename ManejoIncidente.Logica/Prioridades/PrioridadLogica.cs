﻿using ManejoIncidente.Datos.Prioridades;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.Prioridades
{
    public class PrioridadLogica
    {
        private readonly PrioridadContext _prioridadContext;

        public PrioridadLogica()
        {
            _prioridadContext = new PrioridadContext();
        }

        public List<Prioridad> ConsultarPrioridad(int id)
        {
            return _prioridadContext.ConsultarPrioridad(id);
        }

        public void InsertarPrioridad(Prioridad prioridad)
        {
            prioridad.Nombre = string.IsNullOrEmpty(prioridad.Nombre) ? string.Empty : prioridad.Nombre;
            _prioridadContext.InsertarPrioridad(prioridad);
        }

        public void ActualizarPrioridad(Prioridad prioridad)
        {
            prioridad.Nombre = string.IsNullOrEmpty(prioridad.Nombre) ? string.Empty : prioridad.Nombre;
            _prioridadContext.ActualizarPrioridad(prioridad);
        }

        public void EliminarPrioridad(int id)
        {
            _prioridadContext.EliminarPrioridad(id);
        }
    }
}
