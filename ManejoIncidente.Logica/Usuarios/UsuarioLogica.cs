﻿using ManejoIncidente.Datos.Usuarios;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.Usuarios
{
    public class UsuarioLogica
    {
        private readonly UsuarioContext _usuarioContext;
        public UsuarioLogica()
        {
            _usuarioContext = new UsuarioContext();
        }

        public List<Usuario> ConsultarUsuario(int id, string tipo ="")
        {
            return _usuarioContext.ConsultarUsuario(id,tipo);
        }

        public void InsertarUsuario(Usuario usuario)
        {
            usuario.Nombre = string.IsNullOrEmpty(usuario.Nombre) ? string.Empty : usuario.Nombre;
            usuario.NombreUsuario = string.IsNullOrEmpty(usuario.NombreUsuario) ? string.Empty : usuario.NombreUsuario;
            usuario.Apellido = string.IsNullOrEmpty(usuario.Apellido) ? string.Empty : usuario.Apellido;
            usuario.Cedula = string.IsNullOrEmpty(usuario.Cedula) ? string.Empty : usuario.Cedula;
            usuario.Contrasena = string.IsNullOrEmpty(usuario.Contrasena) ? string.Empty : usuario.Contrasena;
            usuario.Correo = string.IsNullOrEmpty(usuario.Correo) ? string.Empty : usuario.Correo;
            usuario.Telefono = string.IsNullOrEmpty(usuario.Telefono) ? string.Empty : usuario.Telefono;
            _usuarioContext.InsertarUsuario(usuario);
        }

        public void ActualizarUsuario(Usuario usuario)
        {
            usuario.Nombre = string.IsNullOrEmpty(usuario.Nombre) ? string.Empty : usuario.Nombre;
            usuario.NombreUsuario = string.IsNullOrEmpty(usuario.NombreUsuario) ? string.Empty : usuario.NombreUsuario;
            usuario.Apellido = string.IsNullOrEmpty(usuario.Apellido) ? string.Empty : usuario.Apellido;
            usuario.Cedula = string.IsNullOrEmpty(usuario.Cedula) ? string.Empty : usuario.Cedula;
            usuario.Contrasena = string.IsNullOrEmpty(usuario.Contrasena) ? string.Empty : usuario.Contrasena;
            usuario.Correo = string.IsNullOrEmpty(usuario.Correo) ? string.Empty : usuario.Correo;
            usuario.Telefono = string.IsNullOrEmpty(usuario.Telefono) ? string.Empty : usuario.Telefono;
            _usuarioContext.ActualizarUsuario(usuario);
        }

        public void EliminarUsuario(int id)
        {
            _usuarioContext.EliminarUsuario(id);
        }

        public int Autentica(string usuario, string clave)
        {
            return _usuarioContext.Autentica(usuario, clave);
        }
    }
}
