﻿using ManejoIncidente.Datos.HistorialIncidentes;
using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Logica.HistorialIncidentes
{
    public class HistorialLogica
    {
        private readonly HistorialIncidenteContext _historialIncidenteContext;

        public HistorialLogica()
        {
            _historialIncidenteContext = new HistorialIncidenteContext();
        }

        public List<HistorialIncidente> ConsultarHistorial(int id)
        {
            return _historialIncidenteContext.ConsultarHistorial(id);
        }

        public void InsertarHistorial(HistorialIncidente historial)
        {
            historial.Comentario = string.IsNullOrEmpty(historial.Comentario) ? string.Empty : historial.Comentario;
            _historialIncidenteContext.InsertarHistorial(historial);
        }

        public void ActualizarHistorial(HistorialIncidente historial)
        {
            historial.Comentario = string.IsNullOrEmpty(historial.Comentario) ? string.Empty : historial.Comentario;
            _historialIncidenteContext.ActualizarHistorial(historial);
        }

        public void EliminarHistorial(int id)
        {
            _historialIncidenteContext.EliminarHistorial(id);
        }
    }
}
