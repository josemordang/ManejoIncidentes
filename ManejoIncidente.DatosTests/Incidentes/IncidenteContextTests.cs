﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.Incidentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Incidentes.Tests
{
    [TestClass()]
    public class IncidenteContextTests
    {
        [TestMethod()]
        public void InsertarIncidenteTest()
        {
            var datos = new Incidente()
            {
                FechaIngreso = DateTimeOffset.Now,
                Descripcion = "descripcion mal",
                CreadoPor = 1,
                DepartamentoId = 6,
                PrioridadId = 1,
                Estatus = "A",
                Titulo = "Tercer Titulo",
                UsuarioAsignadoId = 3,
                UsuarioReportaId = 1,
                ComentarioCierre = string.Empty
            };
            new IncidenteContext().InsertarIncidente(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarIncidenteTest()
        {
            var datos = new Incidente()
            {
                IncidenteId = 3,
                FechaModificacion = DateTimeOffset.Now,
                Descripcion = "Esta es el tercer incidente",
                ModificadoPor = 3,
                DepartamentoId = 6,
                PrioridadId = 1,
                Estatus = "PR",
                Titulo = "Inicente 3",
                UsuarioAsignadoId = 3,
                UsuarioReportaId = 1,
                ComentarioCierre = string.Empty
            };
            new IncidenteContext().ActualizarIncidente(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConsultarIncidentesTest()
        {
            var a = new IncidenteContext().ConsultarIncidentes(1,"");
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarIncidenteTest()
        {
            new IncidenteContext().EliminarIncidente(1);
            Assert.IsTrue(true);
        }
    }
}