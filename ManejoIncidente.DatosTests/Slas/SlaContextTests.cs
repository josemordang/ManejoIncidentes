﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.Slas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Slas.Tests
{
    [TestClass()]
    public class SlaContextTests
    {
        [TestMethod()]
        public void InsertarPuestoTest()
        {
            var datos = new Sla()
            {
                Descripcion = "malisomo",
                CantidadHoras = 7,
                Estatus = "A",
                CreadoPor = 1,
                //ModificadoPor = 1,
                FechaIngreso = DateTimeOffset.Now,
                //FechaModificacion = DateTimeOffset.Now

            };
            new SlaContext().InsertarSla(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarPuestoTest()
        {
            var datos = new Sla()
            {
                SlaId = 5,
                Descripcion = "quinto descripcion.5",
                CantidadHoras = 4,
                Estatus = "A",
                ModificadoPor = 3,
                CreadoPor =3,
                //FechaIngreso = DateTimeOffset.Now,
                FechaModificacion = DateTimeOffset.Now

            };
            new SlaContext().ActualizarSla(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarPuestoTest()
        {
            new SlaContext().EliminarSla(2);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConsultarPuestosTest()
        {
            var a = new SlaContext().ConsultarSla(0);
            Assert.IsTrue(true);
        }
    }
}