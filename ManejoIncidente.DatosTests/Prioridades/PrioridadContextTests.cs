﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.Prioridades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Prioridades.Tests
{
    [TestClass()]
    public class PrioridadContextTests
    {
        [TestMethod()]
        public void InsertarPrioridadTest()
        {
            var datos = new Prioridad()
            {
                SlaId = 1,
                Nombre = "mal3",
                Estatus = "A",
                CreadoPor = 1,
                FechaIngreso = DateTimeOffset.Now

            };
            new PrioridadContext().InsertarPrioridad(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarPrioridadTest()
        {
            var datos = new Prioridad()
            {
                PrioridadId = 4,
                SlaId = 1,
                Nombre = "Cuarta prioridad",
                Estatus = "A",
                ModificadoPor = 3,
                //FechaIngreso = DateTimeOffset.Now,
                FechaModificacion = DateTimeOffset.Now

            };
            new PrioridadContext().ActualizarPrioridad(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConsultarPrioridadTest()
        {
            var a = new PrioridadContext().ConsultarPrioridad(1);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarPrioridadTest()
        {
            new PrioridadContext().EliminarPrioridad(1);
            Assert.IsTrue(true);
        }
    }
}