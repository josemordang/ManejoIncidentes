﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.Departamentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Departamentos.Tests
{
    [TestClass()]
    public class DepartamentoContextTests
    {
        [TestMethod()]
        public void InsertarDepartamentoTest()
        {
            var departameto = new Departamento()
            {
                Nombre = "Nombre 1",
                Estatus = "A",
                Borrado = false,
                CreadoPor = 1,
                FechaIngreso = DateTimeOffset.Now
            };

            new DepartamentoContext().InsertarDepartamento(departameto);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConsultarDepartamentosTest()
        {
            var a = new DepartamentoContext().ConsultarDepartamentos(0);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarDepartamentoTest()
        {
            var departameto = new Departamento()
            {
                DepartamentoId = 7,
                Nombre = " Departamento 7",
                Estatus = "A",
                Borrado = false,
                ModificadoPor = 3,
                FechaModificacion = DateTimeOffset.Now
            };

            new DepartamentoContext().ActualizarDepartamento(departameto);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarDepartamentoTest()
        {
            new DepartamentoContext().EliminarDepartamento(5);
            Assert.IsTrue(true);
        }
    }
}