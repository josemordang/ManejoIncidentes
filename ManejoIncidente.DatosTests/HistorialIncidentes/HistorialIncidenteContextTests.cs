﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.HistorialIncidentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.HistorialIncidentes.Tests
{
    [TestClass()]
    public class HistorialIncidenteContextTests
    {
        [TestMethod()]
        public void InsertarHistorialTest()
        {
            var datos = new HistorialIncidente
            {
                IncidenteId = 1,
                CreadoPor = 1,
                Estatus = "A",
                FechaIngreso = DateTimeOffset.Now,
                Comentario = string.Empty
            };
            new HistorialIncidenteContext().InsertarHistorial(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarHistorialTest()
        {
            var datos = new HistorialIncidente
            {
                HistorialId = 1,
                IncidenteId = 1,
                ModificadoPor = 3,
                Estatus = "A",
                FechaModificacion = DateTimeOffset.Now,
                Comentario = "Cambio"
            };
            new HistorialIncidenteContext().ActualizarHistorial(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConsultarHistorialTest()
        {
            var a = new HistorialIncidenteContext().ConsultarHistorial(0);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarHistorialTest()
        {
            new HistorialIncidenteContext().EliminarHistorial(1);
            Assert.IsTrue(true);
        }
    }
}