﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.Puestos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Puestos.Tests
{
    [TestClass()]
    public class PuestoContextTests
    {
        [TestMethod()]
        public void ConsultarPuestosTest()
        {
            var a = new PuestoContext().ConsultarPuestos(0);

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void InsertarPuestoTest()
        {
            var datos = new Puesto()
            {
                DepartamentoId = 6,
                Estatus = "A",
                CreadoPor = 1,
                ModificadoPor = 1,
                FechaIngreso = DateTimeOffset.Now,
                Nombre = "sadada Departamento"

            };

            new PuestoContext().InsertarPuesto(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarPuestoTest()
        {
            var datos = new Puesto()
            {
                PuestoId = 4,
                DepartamentoId = 5,
                Estatus = "B",
                ModificadoPor = 3,
                FechaModificacion = DateTimeOffset.Now,
                Nombre = "Cuarto Puesto"

            };

            new PuestoContext().ActualizarPuesto(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarPuestoTest()
        {
            new PuestoContext().EliminarPuesto(2);
            Assert.IsTrue(true);
        }
    }
}