﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ManejoIncidente.Datos.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Usuarios.Tests
{
    [TestClass()]
    public class UsuarioContextTests
    {
        [TestMethod()]
        public void InsertarUsuarioTest()
        {
            var datos = new Usuario()
            {
                PuestoId = 2,
                Nombre = "Primer user",
                Apellido = "primer apellido",
                FechaIngreso = DateTimeOffset.Now,
                FechaNacimiento = DateTime.Now,
                Estatus = "A",
                CreadoPor = 1,
                ModificadoPor = 1,
                NombreUsuario = "Puser",
                Cedula = "001001001",
                Contrasena = "calvep",
                Correo = "primercorreo@hotmail.com",
                Telefono = "000000000"
            };
            new UsuarioContext().InsertarUsuario(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ConsultarUsuarioTest()
        {
            var datos = new UsuarioContext().ConsultarUsuario(0,"");
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void ActualizarUsuarioTest()
        {
            var datos = new Usuario()
            {
                PuestoId = 2,
                Nombre = "Joan Mordan User 3",
                Apellido = "Mordan Geronimo",
                FechaModificacion = DateTimeOffset.Now,
                FechaNacimiento = DateTime.Parse("06-12-1986"),
                Estatus = "A",
                ModificadoPor = 3,
                NombreUsuario = "joanUser",
                Cedula = "001001001",
                Contrasena = "123456C",
                Correo = "joan@Gmail.com",
                Telefono = "000000000",
                UsuarioId = 4
            };
            new UsuarioContext().ActualizarUsuario(datos);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EliminarUsuarioTest()
        {
            new UsuarioContext().EliminarUsuario(1);
            Assert.IsTrue(true);
        }
    }
}