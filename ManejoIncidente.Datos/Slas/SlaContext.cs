﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos.Slas
{
    public class SlaContext
    {
        public List<Sla> ConsultarSla(int id)
        {
            var datos = new List<Sla>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (id.Equals(0)) cmd.CommandText = QuerysSla.ConsultarSla;
                else
                {
                    cmd.CommandText = $"{QuerysSla.ConsultarSla} and SlaId = @id";
                    cmd.Parameters.AddWithValue("id", id);
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new Sla
                    {
                        SlaId = dr.IsDBNull(dr.GetOrdinal("SlaId")) ? 0 : int.Parse(dr["SlaId"].ToString()),
                        CantidadHoras = dr.IsDBNull(dr.GetOrdinal("CantidadHoras")) ? 0 : int.Parse(dr["CantidadHoras"].ToString()),
                        Descripcion = dr.IsDBNull(dr.GetOrdinal("Descripcion")) ? string.Empty : dr["Descripcion"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarSla(Sla sla)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysSla.InsertarSla;
                cmd.Parameters.AddWithValue("Descripcion", sla.Descripcion);
                cmd.Parameters.AddWithValue("CantidadHoras", sla.CantidadHoras);
                cmd.Parameters.AddWithValue("Estatus", sla.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", sla.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", sla.CreadoPor);
                //cmd.Parameters.AddWithValue("ModificadoPor", sla.ModificadoPor);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void ActualizarSla(Sla sla)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysSla.ActualizarSla;
                cmd.Parameters.AddWithValue("Descripcion", sla.Descripcion);
                cmd.Parameters.AddWithValue("CantidadHoras", sla.CantidadHoras);
                //cmd.Parameters.AddWithValue("Estatus", sla.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", sla.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", sla.ModificadoPor);
                cmd.Parameters.AddWithValue("id", sla.SlaId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarSla(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysSla.BorrarSla;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
