﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos.Puestos
{
    public class PuestoContext
    {
        public List<Puesto> ConsultarPuestos(int id)
        {
            var datos = new List<Puesto>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (id.Equals(0)) cmd.CommandText = QuerysPuesto.ConsultarPuesto;
                else
                {
                    cmd.CommandText = $"{QuerysPuesto.ConsultarPuesto} and PuestoId = @id";
                    cmd.Parameters.AddWithValue("id", id);
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new Puesto
                    {
                        PuestoId = dr.IsDBNull(dr.GetOrdinal("PuestoId")) ? 0 : int.Parse(dr["PuestoId"].ToString()),
                        DepartamentoId = dr.IsDBNull(dr.GetOrdinal("DepartamentoId")) ? 0 : int.Parse(dr["DepartamentoId"].ToString()),
                        Nombre = dr.IsDBNull(dr.GetOrdinal("Nombre")) ? string.Empty : dr["Nombre"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarPuesto(Puesto puesto)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysPuesto.InsertarPuesto;
                cmd.Parameters.AddWithValue("DepartamentoId", puesto.DepartamentoId);
                cmd.Parameters.AddWithValue("Nombre", puesto.Nombre);
                cmd.Parameters.AddWithValue("Estatus", puesto.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", puesto.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", puesto.CreadoPor);
                //cmd.Parameters.AddWithValue("ModificadoPor", puesto.ModificadoPor);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void ActualizarPuesto(Puesto puesto)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysPuesto.ActualizarPuesto;
                cmd.Parameters.AddWithValue("DepartamentoID", puesto.DepartamentoId);
                cmd.Parameters.AddWithValue("Nombre", puesto.Nombre);
                //cmd.Parameters.AddWithValue("Estatus", puesto.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", puesto.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", puesto.ModificadoPor);
                cmd.Parameters.AddWithValue("id", puesto.PuestoId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarPuesto(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysPuesto.BorrarPuesto;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
