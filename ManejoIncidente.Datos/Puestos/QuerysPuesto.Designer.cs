﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ManejoIncidente.Datos.Puestos {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class QuerysPuesto {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal QuerysPuesto() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ManejoIncidente.Datos.Puestos.QuerysPuesto", typeof(QuerysPuesto).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to update  Puesto
        ///           set 
        ///		   DepartamentoId = @DepartamentoId
        ///		   ,Nombre = @Nombre
        ///           ,FechaModificacion = @FechaModificacion
        ///           ,ModificadoPor = @ModificadoPor
        ///
        ///		   where PuestoId = @id.
        /// </summary>
        internal static string ActualizarPuesto {
            get {
                return ResourceManager.GetString("ActualizarPuesto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to update  Puesto
        ///           set Borrado = @Borrado
        ///		   where PuestoId = @id.
        /// </summary>
        internal static string BorrarPuesto {
            get {
                return ResourceManager.GetString("BorrarPuesto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT PuestoId
        ///      ,DepartamentoId
        ///      ,Nombre
        ///      ,Estatus
        ///      ,Borrado
        ///      ,FechaIngreso
        ///      ,FechaModificacion
        ///      ,CreadoPor
        ///      ,ModificadoPor
        ///  FROM Puesto where borrado =0.
        /// </summary>
        internal static string ConsultarPuesto {
            get {
                return ResourceManager.GetString("ConsultarPuesto", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO Puesto
        ///           (DepartamentoId
        ///		   ,Nombre
        ///           ,Estatus
        ///           ,Borrado
        ///           ,FechaIngreso
        ///           ,CreadoPor)
        ///     VALUES
        ///           (@DepartamentoId  
        ///,@Nombre
        ///           ,@Estatus
        ///           ,@Borrado
        ///           ,@FechaIngreso
        ///           ,@CreadoPor).
        /// </summary>
        internal static string InsertarPuesto {
            get {
                return ResourceManager.GetString("InsertarPuesto", resourceCulture);
            }
        }
    }
}
