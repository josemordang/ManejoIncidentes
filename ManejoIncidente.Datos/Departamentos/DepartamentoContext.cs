﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManejoIncidente.Modelos;

namespace ManejoIncidente.Datos.Departamentos
{
    public class DepartamentoContext
    {
        public List<Departamento> ConsultarDepartamentos(int id)
        {
            var datos = new List<Departamento>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (id.Equals(0)) cmd.CommandText = QueryDepartamento.ConsultaDepartamento;
                else
                {
                    cmd.CommandText = $"{QueryDepartamento.ConsultaDepartamento} and DepartamentoId = @id";
                    cmd.Parameters.AddWithValue("id", id);
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new Departamento
                    {
                        DepartamentoId = dr.IsDBNull(dr.GetOrdinal("DepartamentoId")) ? 0 : int.Parse(dr["DepartamentoId"].ToString()),
                        Nombre = dr.IsDBNull(dr.GetOrdinal("Nombre")) ? string.Empty : dr["Nombre"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarDepartamento(Departamento departamento)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QueryDepartamento.InsertarDepartamento;
                cmd.Parameters.AddWithValue("Nombre", departamento.Nombre);
                cmd.Parameters.AddWithValue("Estatus", departamento.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", departamento.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", departamento.CreadoPor);
                //cmd.Parameters.AddWithValue("ModificadoPor", departamento.ModificadoPor);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void ActualizarDepartamento(Departamento departamento)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QueryDepartamento.ActualizarDepartamento;
                cmd.Parameters.AddWithValue("Nombre", departamento.Nombre);
                //cmd.Parameters.AddWithValue("Estatus", departamento.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", departamento.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", departamento.ModificadoPor);
                cmd.Parameters.AddWithValue("id", departamento.DepartamentoId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarDepartamento(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QueryDepartamento.BorrarDepartamento;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        private SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
