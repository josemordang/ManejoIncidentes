﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos.HistorialIncidentes
{
    public class HistorialIncidenteContext
    {
        public List<HistorialIncidente> ConsultarHistorial(int id)
        {
            var datos = new List<HistorialIncidente>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (id.Equals(0)) cmd.CommandText = QuerysHistorialIncidente.ConsultarHistorialIncidente;
                else
                {
                    cmd.CommandText = $"{QuerysHistorialIncidente.ConsultarHistorialIncidente} and HistorialIncidenteId = @id";
                    cmd.Parameters.AddWithValue("id", id);
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new HistorialIncidente
                    {
                        HistorialId = dr.IsDBNull(dr.GetOrdinal("HistorialIncidenteId")) ? 0 : int.Parse(dr["HistorialIncidenteId"].ToString()),
                        IncidenteId = dr.IsDBNull(dr.GetOrdinal("IncidenteId")) ? 0 : int.Parse(dr["IncidenteId"].ToString()),
                        Comentario = dr.IsDBNull(dr.GetOrdinal("Comentario")) ? string.Empty : dr["Comentario"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarHistorial(HistorialIncidente historialIncidente)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysHistorialIncidente.InsertarHistorialIncidente;
                cmd.Parameters.AddWithValue("IncidenteId", historialIncidente.IncidenteId);
                cmd.Parameters.AddWithValue("Comentario", historialIncidente.Comentario);
                cmd.Parameters.AddWithValue("Estatus", historialIncidente.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", historialIncidente.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", historialIncidente.CreadoPor);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void ActualizarHistorial(HistorialIncidente historialIncidente)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysHistorialIncidente.ActualizarHistorialIncidente;
                cmd.Parameters.AddWithValue("IncidenteId", historialIncidente.IncidenteId);
                cmd.Parameters.AddWithValue("Comentario", historialIncidente.Comentario);
                cmd.Parameters.AddWithValue("Estatus", historialIncidente.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", historialIncidente.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", historialIncidente.ModificadoPor);
                cmd.Parameters.AddWithValue("id", historialIncidente.HistorialId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarHistorial(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysHistorialIncidente.BorrarHistorialIncidente;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        private SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
