﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos.Prioridades
{
    public class PrioridadContext
    {
        public List<Prioridad> ConsultarPrioridad(int id)
        {
            var datos = new List<Prioridad>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (id.Equals(0)) cmd.CommandText = QuerysPrioridad.ConsultarPrioridad;
                else
                {
                    cmd.CommandText = $"{QuerysPrioridad.ConsultarPrioridad} and PrioridadId = @id";
                    cmd.Parameters.AddWithValue("id", id);
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new Prioridad
                    {
                        PrioridadId = dr.IsDBNull(dr.GetOrdinal("PrioridadId")) ? 0 : int.Parse(dr["PrioridadId"].ToString()),
                        SlaId = dr.IsDBNull(dr.GetOrdinal("SlaId")) ? 0 : int.Parse(dr["SlaId"].ToString()),
                        Nombre = dr.IsDBNull(dr.GetOrdinal("Nombre")) ? string.Empty : dr["Nombre"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarPrioridad(Prioridad prioridad)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysPrioridad.InsertarPrioridad;
                cmd.Parameters.AddWithValue("SlaId", prioridad.SlaId);
                cmd.Parameters.AddWithValue("Nombre", prioridad.Nombre);
                cmd.Parameters.AddWithValue("Estatus", prioridad.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", prioridad.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", prioridad.CreadoPor);
                //cmd.Parameters.AddWithValue("ModificadoPor", prioridad.ModificadoPor);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }
        public void ActualizarPrioridad(Prioridad prioridad)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysPrioridad.ActualizarPrioridad;
                cmd.Parameters.AddWithValue("SlaId", prioridad.SlaId);
                cmd.Parameters.AddWithValue("Nombre", prioridad.Nombre);
                //cmd.Parameters.AddWithValue("Estatus", prioridad.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", prioridad.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", prioridad.ModificadoPor);
                cmd.Parameters.AddWithValue("id", prioridad.PrioridadId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarPrioridad(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysPrioridad.BorrarPrioridad;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
