﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos
{
    public class Conexion
    {
        public string ConexionDB()
        {
            var conexion = ConfigurationManager.ConnectionStrings["ConexionSql"];
            return conexion.ConnectionString;
        }
    }
}
