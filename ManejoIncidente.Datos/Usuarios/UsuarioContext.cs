﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos.Usuarios
{
    public class UsuarioContext
    {
        public List<Usuario> ConsultarUsuario(int id, string tipo)
        {
            var datos = new List<Usuario>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (tipo.Equals("Dep"))
                {
                    cmd.CommandText = QuerysUsuarios.UsuarioDepartamento;
                    cmd.Parameters.AddWithValue("dpid", id);
                }
                else if (id.Equals(0)) cmd.CommandText = QuerysUsuarios.ConsultarUsuarios;
                else
                {
                    cmd.CommandText = $"{QuerysUsuarios.ConsultarUsuarios} and usuarioId = @id";
                    cmd.Parameters.AddWithValue("id", id);
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new Usuario
                    {
                        UsuarioId = dr.IsDBNull(dr.GetOrdinal("UsuarioId")) ? 0 : int.Parse(dr["UsuarioId"].ToString()),
                        PuestoId = dr.IsDBNull(dr.GetOrdinal("PuestoId")) ? 0 : int.Parse(dr["PuestoId"].ToString()),
                        Nombre = dr.IsDBNull(dr.GetOrdinal("Nombre")) ? string.Empty : dr["Nombre"].ToString(),
                        Apellido = dr.IsDBNull(dr.GetOrdinal("Apellido")) ? string.Empty : dr["Apellido"].ToString(),
                        Cedula = dr.IsDBNull(dr.GetOrdinal("Cedula")) ? string.Empty : dr["Cedula"].ToString(),
                        Correo = dr.IsDBNull(dr.GetOrdinal("Correo")) ? string.Empty : dr["Correo"].ToString(),
                        Telefono = dr.IsDBNull(dr.GetOrdinal("Telefono")) ? string.Empty : dr["Telefono"].ToString(),
                        FechaNacimiento = dr.IsDBNull(dr.GetOrdinal("FechaNacimiento")) ? new DateTime() : DateTime.Parse(dr["FechaNacimiento"].ToString()),
                        NombreUsuario = dr.IsDBNull(dr.GetOrdinal("NombreUsuario")) ? string.Empty : dr["NombreUsuario"].ToString(),
                        Contrasena = dr.IsDBNull(dr.GetOrdinal("Contrasena")) ? string.Empty : dr["Contrasena"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarUsuario(Usuario usuario)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysUsuarios.InsertarUsuario;
                cmd.Parameters.AddWithValue("PuestoId", usuario.PuestoId);
                cmd.Parameters.AddWithValue("Nombre", usuario.Nombre);
                cmd.Parameters.AddWithValue("Apellido", usuario.Apellido);
                cmd.Parameters.AddWithValue("Cedula", usuario.Cedula);
                cmd.Parameters.AddWithValue("Correo", usuario.Correo);
                cmd.Parameters.AddWithValue("Telefono", usuario.Telefono);
                cmd.Parameters.AddWithValue("FechaNacimiento", usuario.FechaNacimiento);
                cmd.Parameters.AddWithValue("NombreUsuario", usuario.NombreUsuario);
                cmd.Parameters.AddWithValue("Contrasena", usuario.Contrasena);
                cmd.Parameters.AddWithValue("Estatus", usuario.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", usuario.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", usuario.CreadoPor);
                //cmd.Parameters.AddWithValue("ModificadoPor", usuario.ModificadoPor);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void ActualizarUsuario(Usuario usuario)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysUsuarios.ActualizarUsuario;
                cmd.Parameters.AddWithValue("PuestoId", usuario.PuestoId);
                cmd.Parameters.AddWithValue("Nombre", usuario.Nombre);
                cmd.Parameters.AddWithValue("Apellido", usuario.Apellido);
                cmd.Parameters.AddWithValue("Cedula", usuario.Cedula);
                cmd.Parameters.AddWithValue("Correo", usuario.Correo);
                cmd.Parameters.AddWithValue("Telefono", usuario.Telefono);
                cmd.Parameters.AddWithValue("FechaNacimiento", usuario.FechaNacimiento);
                cmd.Parameters.AddWithValue("NombreUsuario", usuario.NombreUsuario);
                cmd.Parameters.AddWithValue("Contrasena", usuario.Contrasena);
                //cmd.Parameters.AddWithValue("Estatus", usuario.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", usuario.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", usuario.ModificadoPor);
                cmd.Parameters.AddWithValue("UsuarioId", usuario.UsuarioId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarUsuario(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysUsuarios.BorrarUsuario;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public int Autentica(string userId, string clave)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysUsuarios.Autentica;
                cmd.Parameters.AddWithValue("userID", userId);
                cmd.Parameters.AddWithValue("clave", clave);
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return 0;
                }
                dr.Read();
                int id = dr.IsDBNull(dr.GetOrdinal("UsuarioId")) ? 0 : int.Parse(dr["UsuarioId"].ToString());
                cnn.Close();
                return id;
            }
        }

        public SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
