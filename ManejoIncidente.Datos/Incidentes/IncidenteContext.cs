﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Datos.Incidentes
{
    public class IncidenteContext
    {
        public List<Incidente> ConsultarIncidentes(int id, string tipo)
        {
            var datos = new List<Incidente>();
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (tipo.Equals("Asignar"))
                {
                    cmd.CommandText = QuerysIncidente.ConsultarAsignar;
                }
                else if (tipo.Equals("Propios"))
                {
                    cmd.CommandText = QuerysIncidente.Propios;
                    cmd.Parameters.AddWithValue("id", id);
                }
                else if (tipo.Equals("Pendientes"))
                {
                    cmd.CommandText = QuerysIncidente.Pendientes;
                    cmd.Parameters.AddWithValue("id", id);
                }
                else if (tipo.Equals("Completados"))
                {
                    cmd.CommandText = QuerysIncidente.Completados;
                    cmd.Parameters.AddWithValue("id", id);
                }
                else if (tipo.Equals("Solicitudes"))
                {
                    cmd.CommandText = QuerysIncidente.IncidentesReportados;
                    cmd.Parameters.AddWithValue("id", id);
                }
                else
                {
                    if (id.Equals(0)) cmd.CommandText = QuerysIncidente.ConsultarIncidente;
                    else
                    {
                        cmd.CommandText = $"{QuerysIncidente.ConsultarIncidente} and IncidenteId = @id";
                        cmd.Parameters.AddWithValue("id", id);
                    }
                }
                var dr = cmd.ExecuteReader();
                if (!dr.HasRows)
                {
                    cnn.Close();
                    return datos;
                }
                while (dr.Read())
                {
                    datos.Add(new Incidente
                    {
                        IncidenteId = dr.IsDBNull(dr.GetOrdinal("IncidenteId")) ? 0 : int.Parse(dr["IncidenteId"].ToString()),
                        UsuarioReportaId = dr.IsDBNull(dr.GetOrdinal("UsuarioReportaId")) ? 0 : int.Parse(dr["UsuarioReportaId"].ToString()),
                        UsuarioAsignadoId = dr.IsDBNull(dr.GetOrdinal("UsuarioAsignadoId")) ? 0 : int.Parse(dr["UsuarioAsignadoId"].ToString()),
                        PrioridadId = dr.IsDBNull(dr.GetOrdinal("PrioridadId")) ? 0 : int.Parse(dr["PrioridadId"].ToString()),
                        DepartamentoId = dr.IsDBNull(dr.GetOrdinal("DepartamentoId")) ? 0 : int.Parse(dr["DepartamentoId"].ToString()),
                        Titulo = dr.IsDBNull(dr.GetOrdinal("Titulo")) ? string.Empty : dr["Titulo"].ToString(),
                        Descripcion = dr.IsDBNull(dr.GetOrdinal("Descripcion")) ? string.Empty : dr["Descripcion"].ToString(),
                        FechaCierre = dr.IsDBNull(dr.GetOrdinal("FechaCierre")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaCierre"].ToString()),
                        ComentarioCierre = dr.IsDBNull(dr.GetOrdinal("ComentarioCierre")) ? string.Empty : dr["ComentarioCierre"].ToString(),
                        Estatus = dr.IsDBNull(dr.GetOrdinal("Estatus")) ? string.Empty : dr["Estatus"].ToString(),
                        Borrado = dr.IsDBNull(dr.GetOrdinal("Borrado")) ? false : bool.Parse(dr["Borrado"].ToString()),
                        CreadoPor = dr.IsDBNull(dr.GetOrdinal("CreadoPor")) ? 0 : int.Parse(dr["CreadoPor"].ToString()),
                        ModificadoPor = dr.IsDBNull(dr.GetOrdinal("ModificadoPor")) ? 0 : int.Parse(dr["ModificadoPor"].ToString()),
                        FechaIngreso = dr.IsDBNull(dr.GetOrdinal("FechaIngreso")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaIngreso"].ToString()),
                        FechaModificacion = dr.IsDBNull(dr.GetOrdinal("FechaModificacion")) ? new DateTimeOffset() : DateTimeOffset.Parse(dr["FechaModificacion"].ToString())
                    });
                }
                cnn.Close();
                return datos;
            }
        }

        public void InsertarIncidente(Incidente incidente)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysIncidente.InsertarIncidente;
                cmd.Parameters.AddWithValue("UsuarioReportaId", incidente.UsuarioReportaId);
                cmd.Parameters.AddWithValue("PrioridadId", incidente.PrioridadId);
                cmd.Parameters.AddWithValue("DepartamentoId", incidente.DepartamentoId);
                cmd.Parameters.AddWithValue("Titulo", incidente.Titulo);
                cmd.Parameters.AddWithValue("Descripcion", incidente.Descripcion);
                //cmd.Parameters.AddWithValue("FechaCierre", incidente.FechaCierre);
                cmd.Parameters.AddWithValue("ComentarioCierre", incidente.ComentarioCierre);
                cmd.Parameters.AddWithValue("Estatus", incidente.Estatus);
                cmd.Parameters.AddWithValue("Borrado", false);
                cmd.Parameters.AddWithValue("FechaIngreso", incidente.FechaIngreso);
                cmd.Parameters.AddWithValue("CreadoPor", incidente.CreadoPor);
                //cmd.Parameters.AddWithValue("ModificadoPor", incidente);

                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void ActualizarIncidente(Incidente incidente)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysIncidente.ActualizarIncidente;
                cmd.Parameters.AddWithValue("PrioridadId", incidente.PrioridadId);
                cmd.Parameters.AddWithValue("DepartamentoId", incidente.DepartamentoId);
                cmd.Parameters.AddWithValue("Titulo", incidente.Titulo);
                cmd.Parameters.AddWithValue("Descripcion", incidente.Descripcion);
                //cmd.Parameters.AddWithValue("FechaCierre", incidente.FechaCierre);
                cmd.Parameters.AddWithValue("ComentarioCierre", incidente.ComentarioCierre);
                //cmd.Parameters.AddWithValue("Estatus", incidente.Estatus);
                cmd.Parameters.AddWithValue("FechaModificacion", incidente.FechaModificacion);
                //cmd.Parameters.AddWithValue("CreadoPor", incidente.CreadoPor);
                cmd.Parameters.AddWithValue("ModificadoPor", incidente.ModificadoPor);
                cmd.Parameters.AddWithValue("id", incidente.IncidenteId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void Asignar(Incidente incidente)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysIncidente.Asignar;
                cmd.Parameters.AddWithValue("UsuarioAsignadoId", incidente.UsuarioAsignadoId);
                cmd.Parameters.AddWithValue("FechaModificacion", incidente.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", incidente.ModificadoPor);
                cmd.Parameters.AddWithValue("Estatus", "AS");
                cmd.Parameters.AddWithValue("Comentario", incidente.ComentarioCierre);
                cmd.Parameters.AddWithValue("id", incidente.IncidenteId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void CambiarEstado(Incidente incidente)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                if (incidente.Estatus.Equals("CE"))
                {
                    cmd.CommandText = QuerysIncidente.Cerrar;
                    cmd.Parameters.AddWithValue("FechaCierre", incidente.FechaCierre);
                }
                else
                {
                    cmd.CommandText = QuerysIncidente.CambiarEstado;
                }

                cmd.Parameters.AddWithValue("FechaModificacion", incidente.FechaModificacion);
                cmd.Parameters.AddWithValue("ModificadoPor", incidente.ModificadoPor);
                cmd.Parameters.AddWithValue("ComentarioCierre", incidente.ComentarioCierre);
                cmd.Parameters.AddWithValue("Estatus", incidente.Estatus);
                cmd.Parameters.AddWithValue("id", incidente.IncidenteId);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public void EliminarIncidente(int id)
        {
            using (var cnn = ObtenerConexion())
            {
                cnn.Open();
                var cmd = cnn.CreateCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = QuerysIncidente.BorrarIncidente;
                cmd.Parameters.AddWithValue("Borrado", true);
                cmd.Parameters.AddWithValue("id", id);
                cmd.ExecuteNonQuery();
                cnn.Close();
            }
        }

        public SqlConnection ObtenerConexion()
        {
            var conexion = new SqlConnection(new Conexion().ConexionDB());
            return conexion;
        }
    }
}
