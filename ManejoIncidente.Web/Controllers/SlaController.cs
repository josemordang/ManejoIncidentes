﻿using ManejoIncidente.Logica.Slas;
using ManejoIncidente.Modelos;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class SlaController : BaseController
    {
        private readonly SlaLogica _slaLogica;
        public SlaController()
        {
            _slaLogica = new SlaLogica();
        }
        // GET: Sla
        [VerifySession]
        public ActionResult Insertar()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                MensajeError = "Error cargando pantalla insertar";
                return RedirectToAction("Slas");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Insertar(SlaVM slaVM)
        {
            try
            {
                var modelo = new Sla()
                {
                    CreadoPor = LoginHelper.ID,
                    CantidadHoras = slaVM.CantidadHoras,
                    Estatus = "A",
                    Descripcion = slaVM.Descripcion,
                    FechaIngreso = DateTimeOffset.Now
                };
                _slaLogica.InsertarSla(modelo);
                MensajeSucces = "Sla Insertado";
                return RedirectToAction("Slas");
            }
            catch (Exception)
            {
                MensajeError = "Erro Insertando";
                return RedirectToAction("Slas");
            }
        }

        [VerifySession]
        public ActionResult Slas()
        {
            try
            {
                var Slas = _slaLogica.ConsultarSla(0);
                var modelo = new List<SlaVM>();
                foreach (var item in Slas)
                {
                    modelo.Add(new SlaVM
                    {
                        SlaId = item.SlaId,
                        CantidadHoras = item.CantidadHoras,
                        Descripcion = item.Descripcion
                    });
                }
                if (Slas.Count.Equals(0)) MensajeInfo = "No Hay Slas Registrados";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla Slas";
                return RedirectToAction("Index", "Home");
            }
        }

        [VerifySession]
        public ActionResult Actualizar(int id)
        {
            try
            {
                var sla = _slaLogica.ConsultarSla(id)[0];
                var modelo = new SlaVM()
                {
                    Descripcion = sla.Descripcion,
                    CantidadHoras = sla.CantidadHoras,
                    SlaId = sla.SlaId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla actualizar";
                return RedirectToAction("Slas");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Actualizar(SlaVM slaVM)
        {
            try
            {
                var modelo = new Sla()
                {
                    ModificadoPor = LoginHelper.ID,
                    CantidadHoras = slaVM.CantidadHoras,
                    Descripcion = slaVM.Descripcion,
                    FechaModificacion = DateTimeOffset.Now,
                    SlaId = slaVM.SlaId
                };
                _slaLogica.ActualizarSla(modelo);
                MensajeSucces = "Sla Actualizado";
                return RedirectToAction("Slas");
            }
            catch (Exception)
            {
                MensajeError = "Error Actualizando";
                return RedirectToAction("Slas");
            }
        }

        [VerifySession]
        public ActionResult Eliminar(int id)
        {
            try
            {
                var sla = _slaLogica.ConsultarSla(id)[0];
                var modelo = new SlaVM()
                {
                    Descripcion = sla.Descripcion,
                    CantidadHoras = sla.CantidadHoras,
                    SlaId = sla.SlaId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla eliminar";
                return RedirectToAction("Slas");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Eliminar(SlaVM slaVM)
        {
            try
            {
                _slaLogica.ElimiarSla(slaVM.SlaId);
                MensajeSucces = "Sla Elimando";
                return RedirectToAction("Slas");
            }
            catch (Exception ex)
            {
                MensajeError = "Error elimnando";
                return RedirectToAction("Slas");
            }
        }
    }
}