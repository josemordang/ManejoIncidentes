﻿using ManejoIncidente.Logica.Prioridades;
using ManejoIncidente.Logica.Slas;
using ManejoIncidente.Modelos;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class PrioridadController : BaseController
    {
        private readonly PrioridadLogica _prioridadLogica;
        private readonly SlaLogica _slaLogica;
        public PrioridadController()
        {
            _prioridadLogica = new PrioridadLogica();
            _slaLogica = new SlaLogica();
        }
        [VerifySession]
        // GET: Prioridad
        public ActionResult Insertar()
        {
            try
            {
                var modelo = new PrioridadVM
                {
                    Slas = _slaLogica.ConsultarSla(0)
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla insertar";
                return RedirectToAction("Prioridades");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Insertar(PrioridadVM prioridadVM)
        {
            try
            {
                var modelo = new Prioridad()
                {
                    CreadoPor = LoginHelper.ID,
                    FechaIngreso = DateTimeOffset.Now,
                    Nombre = prioridadVM.Nombre,
                    SlaId = prioridadVM.SlaId,
                    Estatus = "A"
                };
                _prioridadLogica.InsertarPrioridad(modelo);
                MensajeSucces = "Priodidad Insertada";
                return RedirectToAction("Prioridades");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Insertando";
                return RedirectToAction("Prioridades");
            }
        }

        [VerifySession]
        public ActionResult Prioridades()
        {
            try
            {
                var prioridades = _prioridadLogica.ConsultarPrioridad(0);
                var modelo = new List<PrioridadVM>();
                string sla = "";
                foreach (var item in prioridades)
                {

                    var slas = _slaLogica.ConsultarSla(item.SlaId);
                    if (slas.Count.Equals(0)) sla = "No disponible";
                    else sla = slas[0].Descripcion;
                    modelo.Add(new PrioridadVM
                    {
                        Sla = sla,
                        Nombre = item.Nombre,
                        PrioridadId = item.PrioridadId
                    });
                }
                if (prioridades.Count.Equals(0)) MensajeInfo = "No Hay Prioridades Registradas";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Prioridades";
                return RedirectToAction("Index", "Home");
            }
        }

        [VerifySession]
        public ActionResult Actualizar(int id)
        {
            try
            {
                var prioridad = _prioridadLogica.ConsultarPrioridad(id)[0];
                var slas = _slaLogica.ConsultarSla(0);
                var modelo = new PrioridadVM()
                {
                    Nombre = prioridad.Nombre,
                    Slas = slas,
                    SlaId = prioridad.SlaId,
                    PrioridadId = prioridad.PrioridadId
                };

                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla actualizar";
                return RedirectToAction("Puestos");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Actualizar(PrioridadVM prioridadVM)
        {
            try
            {
                var modelo = new Prioridad()
                {
                    PrioridadId = prioridadVM.PrioridadId,
                    Nombre = prioridadVM.Nombre,
                    SlaId = prioridadVM.SlaId,
                    ModificadoPor = LoginHelper.ID,
                    FechaModificacion = DateTimeOffset.Now
                };
                _prioridadLogica.ActualizarPrioridad(modelo);
                MensajeSucces = "Prioridad Actulizada";
                return RedirectToAction("Prioridades");
            }
            catch (Exception ex)
            {
                MensajeError = "Error actualizando";
                return RedirectToAction("Prioridades");
            }
        }

        [VerifySession]
        public ActionResult Eliminar(int id)
        {
            try
            {
                var prioridad = _prioridadLogica.ConsultarPrioridad(id)[0];
                var slas = _slaLogica.ConsultarSla(prioridad.SlaId);
                string sla = "";
                if (slas.Count.Equals(0)) sla = "No disponible";
                else sla = slas[0].Descripcion;
                var modelo = new PrioridadVM()
                {
                    Nombre = prioridad.Nombre,
                    Sla = sla,
                    PrioridadId = prioridad.PrioridadId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Eliminar";
                return RedirectToAction("Prioridades");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Eliminar(PrioridadVM prioridadVM)
        {
            try
            {
                _prioridadLogica.EliminarPrioridad(prioridadVM.PrioridadId);
                MensajeSucces = "Prioridad Elimanda";
                return RedirectToAction("Prioridades");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Eliminando";
                return RedirectToAction("Prioridades");
            }
        }

    }
}