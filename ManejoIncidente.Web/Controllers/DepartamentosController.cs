﻿using ManejoIncidente.Logica.Departamentos;
using ManejoIncidente.Modelos;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class DepartamentosController : BaseController
    {
        private readonly DepartamentoLogica _departamentoLogica;

        public DepartamentosController()
        {
            _departamentoLogica = new DepartamentoLogica();
        }

        [VerifySession]
        // GET: Departamentos
        public ActionResult Insertar()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla insertar";
                return RedirectToAction("Departamentos");
            }
        }

        [HttpPost, VerifySession]
        // GET: Departamentos
        public ActionResult Insertar(DepartamentoVM departamentoVM)
        {
            try
            {
                var modelo = new Departamento()
                {
                    Nombre = departamentoVM.Nombre,
                    Estatus = "A",
                    FechaIngreso = DateTimeOffset.Now,
                    CreadoPor = LoginHelper.ID
                };
                _departamentoLogica.InsertarDepartamento(modelo);
                MensajeSucces = "Departamento Insertado";
                return RedirectToAction("Departamentos");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Insertando Departamento";
                return RedirectToAction("Departamentos");
            }
        }

        [VerifySession]
        public ActionResult Departamentos()
        {
            try
            {
                var departametos = _departamentoLogica.ConsultaDepartamento(0);
                var modelo = new List<DepartamentoVM>();
                foreach (var item in departametos)
                {
                    modelo.Add(new DepartamentoVM
                    {
                        Nombre = item.Nombre,
                        DepartamentoId = item.DepartamentoId
                    });
                }
                if (departametos.Count.Equals(0)) MensajeInfo = "No Hay Departamentos Registrados";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Departamentos";
                return RedirectToAction("Index", "Home");
            }
        }

        [VerifySession]
        public ActionResult Actualizar(int id)
        {
            try
            {
                var departameto = _departamentoLogica.ConsultaDepartamento(id)[0];
                var modelo = new DepartamentoVM()
                {
                    Nombre = departameto.Nombre,
                    DepartamentoId = departameto.DepartamentoId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de actualizar";
                return RedirectToAction("Departamentos");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Actualizar(DepartamentoVM departamentoVM)
        {
            try
            {
                var modelo = new Departamento()
                {
                    DepartamentoId = departamentoVM.DepartamentoId,
                    Nombre = departamentoVM.Nombre,
                    ModificadoPor = LoginHelper.ID,
                    FechaModificacion = DateTimeOffset.Now
                };
                _departamentoLogica.ActualizarDepartamento(modelo);
                MensajeSucces = "Departamento Actualizado";
                return RedirectToAction("Departamentos");
            }
            catch (Exception ex)
            {
                MensajeError = "Error actualizando";
                return RedirectToAction("Departamentos");
            }
        }

        [VerifySession]
        public ActionResult Eliminar(int id)
        {
            try
            {
                var departameto = _departamentoLogica.ConsultaDepartamento(id)[0];
                var modelo = new DepartamentoVM()
                {
                    Nombre = departameto.Nombre,
                    DepartamentoId = departameto.DepartamentoId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Eliminar";
                return RedirectToAction("Departamentos");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Eliminar(DepartamentoVM departamentoVM)
        {
            try
            {
                _departamentoLogica.EliminarDepartamento(departamentoVM.DepartamentoId);
                MensajeSucces = "Departamento Eliminado";
                return RedirectToAction("Departamentos");
            }
            catch (Exception ex)
            {
                MensajeError = "Error eliminando";
                return RedirectToAction("Departamentos");
            }
        }
    }
}