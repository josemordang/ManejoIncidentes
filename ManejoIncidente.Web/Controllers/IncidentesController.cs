﻿using ManejoIncidente.Logica.Departamentos;
using ManejoIncidente.Logica.HistorialIncidentes;
using ManejoIncidente.Logica.Incidentes;
using ManejoIncidente.Logica.Prioridades;
using ManejoIncidente.Logica.Usuarios;
using ManejoIncidente.Modelos;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class IncidentesController : BaseController
    {
        private readonly IncidenteLogica _incidenteLogica;
        private readonly UsuarioLogica _usuarioLogica;
        private readonly DepartamentoLogica _departamentoLogica;
        private readonly PrioridadLogica _prioridadLogica;

        public IncidentesController()
        {
            _incidenteLogica = new IncidenteLogica();
            _prioridadLogica = new PrioridadLogica();
            _usuarioLogica = new UsuarioLogica();
            _departamentoLogica = new DepartamentoLogica();
        }
        // GET: Incidentes
        [VerifySession]
        public ActionResult Registrar()
        {
            try
            {
                var modelo = new IncidenteVM()
                {
                    Prioridades = _prioridadLogica.ConsultarPrioridad(0),
                    //Usuarios = _usuarioLogica.ConsultarUsuario(0),
                    Departamentos = _departamentoLogica.ConsultaDepartamento(0)
                };

                return View(modelo);
            }
            catch (Exception)
            {
                MensajeError = "Error pantalla de registrar";
                return RedirectToAction("Incidentes");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Registrar(IncidenteVM incidenteVM)
        {
            try
            {
                var incidente = new Incidente()
                {
                    CreadoPor = LoginHelper.ID,
                    UsuarioReportaId = LoginHelper.ID,
                    Estatus = "CR",
                    FechaIngreso = DateTimeOffset.Now,
                    Descripcion = incidenteVM.Descripcion,
                    DepartamentoId = incidenteVM.DepartamentoId,
                    PrioridadId = incidenteVM.PrioridadId,
                    Titulo = incidenteVM.Titulo,
                    ComentarioCierre =incidenteVM.ComentarioCierre
                    //UsuarioAsignadoId = incidenteVM.UsuarioAsignadoId                    
                };
                _incidenteLogica.InsertarIncidente(incidente);
                MensajeSucces = "Incidente Registado";
                return RedirectToAction("Incidentes");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Resgistrando";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Incidentes()
        {
            try
            {
                var incidentes = _incidenteLogica.ConsultarIncidente(0);
                var modelo = new List<IncidenteVM>();
                foreach (var item in incidentes)
                {
                    modelo.Add(new IncidenteVM
                    {
                        Estatus = item.Estatus,
                        Titulo = item.Titulo,
                        Descripcion = item.Descripcion,
                        IncidenteId = item.IncidenteId
                    });
                }
                if (incidentes.Count.Equals(0)) MensajeInfo = "No Hay Incidentes Registrados";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();

                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error pantalla Incidentes";
                return RedirectToAction("Index", "Home");
            }
        }

        [VerifySession]
        public ActionResult Actualizar(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];
                var modelo = new IncidenteVM
                {
                    PrioridadId = incidente.PrioridadId,
                    DepartamentoId = incidente.DepartamentoId,
                    Titulo = incidente.Titulo,
                    IncidenteId = incidente.IncidenteId,
                    Descripcion = incidente.Descripcion,
                    Prioridades = _prioridadLogica.ConsultarPrioridad(0),
                    ComentarioCierre = incidente.ComentarioCierre,
                    //Usuarios = _usuarioLogica.ConsultarUsuario(0),
                    Departamentos = _departamentoLogica.ConsultaDepartamento(0)
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error pantalla de actualizar";
                return RedirectToAction("Incidentes");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Actualizar(IncidenteVM incidenteVM)
        {
            try
            {
                var modelo = new Incidente()
                {
                    IncidenteId = incidenteVM.IncidenteId,
                    Descripcion = incidenteVM.Descripcion,
                    DepartamentoId = incidenteVM.DepartamentoId,
                    FechaModificacion = DateTimeOffset.Now,
                    ModificadoPor = LoginHelper.ID,
                    PrioridadId = incidenteVM.PrioridadId,
                    Titulo = incidenteVM.Titulo,
                    ComentarioCierre =incidenteVM.ComentarioCierre
                };
                _incidenteLogica.ActualizarIncidente(modelo);
                MensajeSucces = "Incidente Actualizado";
                return RedirectToAction("Incidentes");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Actualizando";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Detalles(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];

                var modelo = new IncidenteVM
                {
                    Titulo = incidente.Titulo,
                    Descripcion = incidente.Descripcion,
                    PrioridadI = _prioridadLogica.ConsultarPrioridad(incidente.PrioridadId)[0].Nombre,
                    DepartamentoI = _departamentoLogica.ConsultaDepartamento(incidente.DepartamentoId)[0].Nombre,
                    IncidenteId = incidente.IncidenteId,
                    Estatus = incidente.Estatus,
                    ComentarioCierre = incidente.ComentarioCierre
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de detalles";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Eliminar(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];

                var modelo = new IncidenteVM
                {
                    Titulo = incidente.Titulo,
                    Descripcion = incidente.Descripcion,
                    PrioridadI = _prioridadLogica.ConsultarPrioridad(incidente.PrioridadId)[0].Nombre,
                    DepartamentoI = _departamentoLogica.ConsultaDepartamento(incidente.DepartamentoId)[0].Nombre,
                    IncidenteId = incidente.IncidenteId,
                    Estatus = incidente.Estatus,
                    ComentarioCierre = incidente.ComentarioCierre
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando de eliminar";
                return RedirectToAction("Incidentes");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Eliminar(IncidenteVM incidenteVM)
        {
            try
            {
                _incidenteLogica.EliminarIncidente(incidenteVM.IncidenteId, LoginHelper.ID);
                MensajeSucces = "Incidente Eliminado";
                return RedirectToAction("Incidentes");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Eliminado";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult InidentesPendientes()
        {
            try
            {
                var incidentes = _incidenteLogica.ConsultarIncidente(0, "Asignar");
                var modelo = new List<IncidenteVM>();
                foreach (var item in incidentes)
                {
                    modelo.Add(new IncidenteVM
                    {
                        Estatus = item.Estatus,
                        Titulo = item.Titulo,
                        Descripcion = item.Descripcion,
                        IncidenteId = item.IncidenteId
                    });
                }
                if (incidentes.Count.Equals(0)) MensajeInfo = "No Hay Incidentes Por Asignar";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error consultando los Incidentes Pendientes";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Asignar(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];
                var modelo = new IncidenteVM()
                {
                    Usuarios = _usuarioLogica.ConsultarUsuario(incidente.DepartamentoId,"Dep"),
                    Titulo = incidente.Titulo,
                    Descripcion = incidente.Descripcion,
                    IncidenteId = incidente.IncidenteId,
                    ComentarioCierre= incidente.ComentarioCierre
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargadno pantalla de asignar";
                return RedirectToAction("InidentesPendientes");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Asignar(IncidenteVM incidenteVM)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(incidenteVM.IncidenteId)[0];
                var modelo = new Incidente()
                {
                    UsuarioAsignadoId = incidenteVM.UsuarioAsignadoId,
                    IncidenteId = incidenteVM.IncidenteId,
                    Descripcion = incidente.Descripcion,
                    DepartamentoId = incidente.DepartamentoId,
                    FechaModificacion = DateTimeOffset.Now,
                    ModificadoPor = LoginHelper.ID,
                    PrioridadId = incidente.PrioridadId,
                    Titulo = incidente.Titulo,
                    ComentarioCierre=incidenteVM.ComentarioCierre
                };
                _incidenteLogica.Asignar(modelo);
                MensajeSucces = "Incidente asignado";
                return RedirectToAction("InidentesPendientes");
            }
            catch (Exception ex)
            {
                MensajeError = "Error asignando";
                return RedirectToAction("InidentesPendientes");
            }
        }

        [VerifySession]
        public ActionResult Propios()
        {
            try
            {
                var incidentes = _incidenteLogica.ConsultarIncidente(LoginHelper.ID, "Propios");
                var modelo = new List<IncidenteVM>();
                foreach (var item in incidentes)
                {
                    modelo.Add(new IncidenteVM
                    {
                        Estatus = item.Estatus,
                        Titulo = item.Titulo,
                        Descripcion = item.Descripcion,
                        IncidenteId = item.IncidenteId
                    });
                }
                if (incidentes.Count.Equals(0)) MensajeInfo = "No Hay Incidentes Asignados A Mi";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se logro consultar los incidentes Propios";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Cerrar(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];
                var modelo = new IncidenteVM()
                {
                    Titulo = incidente.Titulo,
                    Descripcion = incidente.Descripcion,
                    IncidenteId = incidente.IncidenteId,
                    Estatus = "Cerrado",
                    ComentarioCierre=incidente.ComentarioCierre
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se cargar la pantalla de cerrar";
                return RedirectToAction("Propios");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Cerrar(IncidenteVM incidenteVM)
        {
            try
            {
                var incidente = new Incidente()
                {
                    IncidenteId = incidenteVM.IncidenteId,
                    FechaModificacion = DateTimeOffset.Now,
                    ModificadoPor = LoginHelper.ID,
                    Estatus = "CE",
                    ComentarioCierre = incidenteVM.ComentarioCierre,
                    FechaCierre = DateTimeOffset.Now,
                };
                _incidenteLogica.CambiarEstado(incidente);
                MensajeSucces = "Incidente Cerrado";
                return RedirectToAction("Propios");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Cerrando";
                return RedirectToAction("Propios");
            }
        }

        [VerifySession]
        public ActionResult Cambiar(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];
                var modelo = new IncidenteVM()
                {
                    Titulo = incidente.Titulo,
                    Descripcion = incidente.Descripcion,
                    IncidenteId = incidente.IncidenteId,
                    Estatus = incidente.Estatus,
                    ComentarioCierre = incidente.ComentarioCierre
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se cargar la pantalla de Cambio";
                return RedirectToAction("Propios");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Cambiar(IncidenteVM incidenteVM)
        {
            try
            {
                var incidente = new Incidente()
                {
                    IncidenteId = incidenteVM.IncidenteId,
                    FechaModificacion = DateTimeOffset.Now,
                    ModificadoPor = LoginHelper.ID,
                    Estatus = incidenteVM.Estatus,
                    ComentarioCierre = incidenteVM.ComentarioCierre
                };
                _incidenteLogica.CambiarEstado(incidente);
                MensajeSucces = "Incidente Cambiado";
                return RedirectToAction("Propios");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Cambiando";
                return RedirectToAction("Propios");
            }
        }

        [VerifySession]
        public ActionResult Pendientes()
        {
            try
            {
                var incidentes = _incidenteLogica.ConsultarIncidente(LoginHelper.ID, "Pendientes");
                var modelo = new List<IncidenteVM>();
                foreach (var item in incidentes)
                {
                    modelo.Add(new IncidenteVM
                    {
                        Estatus = item.Estatus,
                        Titulo = item.Titulo,
                        Descripcion = item.Descripcion,
                        IncidenteId = item.IncidenteId
                    });
                }
                if (incidentes.Count.Equals(0)) MensajeInfo = "No Hay Incidentes Pendientes";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se logro consultar los incidentes Pendientes";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Completados()
        {
            try
            {
                var incidentes = _incidenteLogica.ConsultarIncidente(LoginHelper.ID, "Completados");
                var modelo = new List<IncidenteVM>();
                foreach (var item in incidentes)
                {
                    modelo.Add(new IncidenteVM
                    {
                        Estatus = item.Estatus,
                        Titulo = item.Titulo,
                        Descripcion = item.Descripcion,
                        IncidenteId = item.IncidenteId
                    });
                }
                if (incidentes.Count.Equals(0)) MensajeInfo = "No Hay Incidentes Completados";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se logro consultar los incidentes Completados";
                return RedirectToAction("Incidentes");
            }
        }

        [VerifySession]
        public ActionResult Reabrir(int id)
        {
            try
            {
                var incidente = _incidenteLogica.ConsultarIncidente(id)[0];
                var modelo = new IncidenteVM()
                {
                    Titulo = incidente.Titulo,
                    Descripcion = incidente.Descripcion,
                    IncidenteId = incidente.IncidenteId,
                    Estatus = "Asignado",
                    ComentarioCierre = incidente.ComentarioCierre
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se cargar la pantalla de reabrir";
                return RedirectToAction("Propios");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Reabrir(IncidenteVM incidenteVM)
        {
            try
            {
                var incidente = new Incidente()
                {
                    IncidenteId = incidenteVM.IncidenteId,
                    FechaModificacion = DateTimeOffset.Now,
                    ModificadoPor = LoginHelper.ID,
                    Estatus = "AS",
                    ComentarioCierre = incidenteVM.ComentarioCierre
                };
                _incidenteLogica.CambiarEstado(incidente);
                MensajeSucces = "Incidente Reabierto";
                return RedirectToAction("Completados");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Reabriendo";
                return RedirectToAction("Propios");
            }
        }

        [VerifySession]
        public ActionResult Solicutudes()
        {
            try
            {
                var incidentes = _incidenteLogica.ConsultarIncidente(LoginHelper.ID, "Solicitudes");
                var modelo = new List<IncidenteVM>();
                foreach (var item in incidentes)
                {
                    var prioridad = _prioridadLogica.ConsultarPrioridad(item.PrioridadId);
                    var departamento = _departamentoLogica.ConsultaDepartamento(item.DepartamentoId);
                    var usuario = _usuarioLogica.ConsultarUsuario((item.UsuarioAsignadoId).Equals(0) ? -55 : item.UsuarioAsignadoId);
                    //string fechacierre = (item.FechaCierre.Equals(new DateTimeOffset()) ? string.Empty : item.FechaCierre.ToString("dd/M/yyyy h:m:ss");
                    modelo.Add(new IncidenteVM
                    {
                        Estatus = item.Estatus,
                        Titulo = item.Titulo,
                        Descripcion = item.Descripcion,
                        IncidenteId = item.IncidenteId,
                        FechaCierre = item.FechaCierre,
                        ComentarioCierre = item.ComentarioCierre,
                        PrioridadI = (prioridad.Count.Equals(0)) ? "N/D" : prioridad[0].Nombre,
                        DepartamentoI = (departamento.Count.Equals(0)) ? "N/D" : departamento[0].Nombre,
                        Usuario = (usuario.Count.Equals(0)) ? "N/D" : usuario[0].Nombre
                    });
                }
                if (incidentes.Count.Equals(0)) MensajeInfo = "No Hay Solicitudes";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "No se logro consultar las soliciudes";
                return RedirectToAction("Incidentes");
            }
        }
    }
}