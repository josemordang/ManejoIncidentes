﻿using ManejoIncidente.Logica.Puestos;
using ManejoIncidente.Logica.Usuarios;
using ManejoIncidente.Modelos;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class UsuariosController : BaseController
    {
        private readonly UsuarioLogica _usuarioLogica;
        private readonly PuestoLogica _puestoLogica;
        //public int UsuarioSeccion { get; set; }
        public UsuariosController()
        {
            _usuarioLogica = new UsuarioLogica();
            _puestoLogica = new PuestoLogica();
        }
        // GET: Usuarios
        [VerifySession]
        public ActionResult InsertarUsuario()
        {
            try
            {
                var modelo = new UsuarioVM();
                modelo.Puestos = _puestoLogica.ConsultarPuesto(0);
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Insertar";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult InsertarUsuario(UsuarioVM usuarioVM)
        {
            try
            {
                if (!IsValidEmail(usuarioVM.Correo))
                {
                    MensajeError = "Correo Invalido";
                    usuarioVM.Puestos = _puestoLogica.ConsultarPuesto(0);
                    return View(usuarioVM);
                }
                var usuario = new Usuario()
                {
                    Nombre = usuarioVM.Nombre,
                    Apellido = usuarioVM.Apellido,
                    Cedula = usuarioVM.Cedula,
                    Contrasena = usuarioVM.Contrasena,
                    Correo = usuarioVM.Correo,
                    CreadoPor = LoginHelper.ID,
                    FechaIngreso = DateTimeOffset.Now,
                    Estatus = "A",
                    NombreUsuario = usuarioVM.NombreUsuario,
                    PuestoId = usuarioVM.PuestoId,
                    Telefono = usuarioVM.Telefono,
                    FechaNacimiento = usuarioVM.FechaNacimiento,
                };
                _usuarioLogica.InsertarUsuario(usuario);
                MensajeSucces = "Usuario Insertado";
                return RedirectToAction("Usuarios");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Insertando";
                return RedirectToAction("Usuarios");
            }
        }

        [VerifySession]
        public ActionResult Usuarios()
        {
            try
            {
                var usuarios = _usuarioLogica.ConsultarUsuario(0).Where(x=>x.UsuarioId!=15).ToList();
                List<UsuarioVM> modelo = new List<UsuarioVM>();
                foreach (var item in usuarios)
                {
                    modelo.Add(new UsuarioVM
                    {
                        UsuarioId = item.UsuarioId,
                        Apellido = item.Apellido,
                        Cedula = item.Cedula,
                        //Contrasena = item.Contrasena,
                        //Correo = item.Correo,
                        NombreUsuario = item.NombreUsuario,
                        Nombre = item.Nombre
                    });
                }
                if (usuarios.Count.Equals(0)) MensajeInfo = "No Hay Usuarios Registrados";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception)
            {
                ViewData["ERROR"] = "Error cargando pantalla de Usuarios";
                return RedirectToAction("Index", "Home");
            }

        }

        [VerifySession]
        public ActionResult ActualizarUsuario(int id)
        {
            try
            {
                var usuario = _usuarioLogica.ConsultarUsuario(id)[0];
                var modelo = new UsuarioVM()
                {
                    Nombre = usuario.Nombre,
                    Apellido = usuario.Apellido,
                    Cedula = usuario.Cedula,
                    Contrasena = usuario.Contrasena,
                    Correo = usuario.Correo,
                    FechaNacimiento = usuario.FechaNacimiento,
                    PuestoId = usuario.PuestoId,
                    NombreUsuario = usuario.NombreUsuario,
                    Telefono = usuario.Telefono,
                    Puestos = _puestoLogica.ConsultarPuesto(0),
                    UsuarioId = usuario.UsuarioId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Actualizar";
                return RedirectToAction("Usuarios");

            }
        }

        [HttpPost, VerifySession]
        public ActionResult ActualizarUsuario(UsuarioVM usuarioVM)
        {
            try
            {
                if (!IsValidEmail(usuarioVM.Correo))
                {

                    MensajeError = "Correo Invalido";
                    usuarioVM.Puestos = _puestoLogica.ConsultarPuesto(0);
                    return View(usuarioVM);
                }
                var usuario = new Usuario()
                {
                    UsuarioId = usuarioVM.UsuarioId,
                    Nombre = usuarioVM.Nombre,
                    Apellido = usuarioVM.Apellido,
                    Cedula = usuarioVM.Cedula,
                    Contrasena = usuarioVM.Contrasena,
                    Correo = usuarioVM.Correo,
                    ModificadoPor = LoginHelper.ID,
                    FechaModificacion = DateTimeOffset.Now,
                    NombreUsuario = usuarioVM.NombreUsuario,
                    PuestoId = usuarioVM.PuestoId,
                    Telefono = usuarioVM.Telefono,
                    FechaNacimiento = usuarioVM.FechaNacimiento,
                };
                _usuarioLogica.ActualizarUsuario(usuario);
                MensajeSucces = "Usuario Actualizado";
                return RedirectToAction("Usuarios");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Actualziandno";
                return RedirectToAction("Usuarios");
            }
        }

        [VerifySession]
        public ActionResult DetallesUsuario(int id)
        {
            try
            {
                var usuario = _usuarioLogica.ConsultarUsuario(id)[0];
                var modelo = new UsuarioVM()
                {
                    Nombre = usuario.Nombre,
                    Apellido = usuario.Apellido,
                    Cedula = usuario.Cedula,
                    Contrasena = usuario.Contrasena,
                    Correo = usuario.Correo,
                    FechaNacimiento = usuario.FechaNacimiento,
                    PuestoId = usuario.PuestoId,
                    NombreUsuario = usuario.NombreUsuario,
                    Telefono = usuario.Telefono,
                    Puestos = _puestoLogica.ConsultarPuesto(0),
                    UsuarioId = usuario.UsuarioId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Detalles";
                return RedirectToAction("Usuarios");
            }
        }

        [VerifySession]
        public ActionResult EliminarUsuario(int id)
        {
            try
            {
                var usuario = _usuarioLogica.ConsultarUsuario(id)[0];
                var modelo = new UsuarioVM()
                {
                    Nombre = usuario.Nombre,
                    Apellido = usuario.Apellido,
                    Cedula = usuario.Cedula,
                    Contrasena = usuario.Contrasena,
                    Correo = usuario.Correo,
                    FechaNacimiento = usuario.FechaNacimiento,
                    PuestoId = usuario.PuestoId,
                    NombreUsuario = usuario.NombreUsuario,
                    Telefono = usuario.Telefono,
                    Puestos = _puestoLogica.ConsultarPuesto(0),
                    UsuarioId = usuario.UsuarioId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Eliminar";
                return RedirectToAction("Usuarios");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult EliminarUsuario(UsuarioVM usuarioVM)
        {
            try
            {
                _usuarioLogica.EliminarUsuario(usuarioVM.UsuarioId);
                MensajeSucces = "Usuario Elimiando";
                return RedirectToAction("Usuarios");
            }
            catch (Exception ex)
            {
                MensajeError = "Error Eliminado";
                return RedirectToAction("Usuarios");
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}