﻿using ManejoIncidente.Logica.Departamentos;
using ManejoIncidente.Logica.Puestos;
using ManejoIncidente.Modelos;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class PuestosController : BaseController
    {
        private readonly PuestoLogica _puestoLogica;
        private readonly DepartamentoLogica _departamentoLogica;

        public PuestosController()
        {
            _puestoLogica = new PuestoLogica();
            _departamentoLogica = new DepartamentoLogica();
        }
        // GET: Puestos
        [VerifySession]
        public ActionResult Insertar()
        {
            try
            {
                var departamnetos = _departamentoLogica.ConsultaDepartamento(0);
                var modelo = new PuestoVM()
                {
                    Departamentos = departamnetos
                };

                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla insertar";
                return RedirectToAction("Puestos");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Insertar(PuestoVM puestoVM)
        {
            try
            {
                var modelo = new Puesto()
                {
                    Nombre = puestoVM.Nombre,
                    DepartamentoId = puestoVM.DepartamentoId,
                    Estatus = "A",
                    CreadoPor = LoginHelper.ID,
                    FechaIngreso = DateTimeOffset.Now
                };
                _puestoLogica.InsertarPuesto(modelo);
                MensajeSucces = "Puesto Insertado";
                return RedirectToAction("Puestos");
            }
            catch (Exception)
            {
                MensajeError = "Error Insertando";
                return RedirectToAction("Puestos");
            }
        }

        [VerifySession]
        public ActionResult Puestos()
        {
            try
            {
                var puestos = _puestoLogica.ConsultarPuesto(0);
                var modelo = new List<PuestoVM>();
                string departamento = "";
                foreach (var item in puestos)
                {

                    var departamentos = _departamentoLogica.ConsultaDepartamento(item.DepartamentoId);
                    if (departamentos.Count.Equals(0)) departamento = "No disponible";
                    else departamento = departamentos[0].Nombre;
                    modelo.Add(new PuestoVM
                    {
                        Departamento = departamento,
                        Nombre = item.Nombre,
                        DepartamentoId = item.DepartamentoId,
                        PuestoId = item.PuestoId
                    });
                }
                if (puestos.Count.Equals(0)) MensajeInfo = "No Hay Puestos Registrados";
                ViewData["GOOD"] = MensajeSucces;
                ViewData["ERROR"] = MensajeError;
                ViewData["INFO"] = MensajeInfo;
                LiberarMensajes();
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Puestos";
                return RedirectToAction("Index", "Home");
            }
        }

        [VerifySession]
        public ActionResult Actualizar(int id)
        {
            try
            {
                var departamnetos = _departamentoLogica.ConsultaDepartamento(0);
                var puesto = _puestoLogica.ConsultarPuesto(id)[0];
                var modelo = new PuestoVM()
                {
                    Nombre = puesto.Nombre,
                    Departamentos = departamnetos,
                    PuestoId = puesto.PuestoId,
                    DepartamentoId= puesto.DepartamentoId
                };

                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Actualizar";
                return RedirectToAction("Puestos");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Actualizar(PuestoVM puestoVM)
        {
            try
            {
                var modelo = new Puesto()
                {
                    PuestoId = puestoVM.PuestoId,
                    Nombre = puestoVM.Nombre,
                    DepartamentoId = puestoVM.DepartamentoId,
                    ModificadoPor = LoginHelper.ID,
                    FechaModificacion = DateTimeOffset.Now
                };
                _puestoLogica.ActualizarPuesto(modelo);
                MensajeSucces = "Puesto Actualizado";
                return RedirectToAction("Puestos");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Puestos");
            }
        }

        [VerifySession]
        public ActionResult Eliminar(int id)
        {
            try
            {

                var puesto = _puestoLogica.ConsultarPuesto(id)[0];
                var departamentos = _departamentoLogica.ConsultaDepartamento(puesto.DepartamentoId);
                string departamento = "";
                if (departamentos.Count.Equals(0)) departamento = "No disponible";
                else departamento = departamentos[0].Nombre;
                var modelo = new PuestoVM()
                {
                    Nombre = puesto.Nombre,
                    Departamento = departamento,
                    PuestoId = puesto.PuestoId
                };
                return View(modelo);
            }
            catch (Exception ex)
            {
                MensajeError = "Error cargando pantalla de Eliminar";
                return RedirectToAction("Puestos");
            }
        }

        [HttpPost, VerifySession]
        public ActionResult Eliminar(PuestoVM puestoVM)
        {
            try
            {
                _puestoLogica.EliminarPuesto(puestoVM.PuestoId);
                MensajeSucces = "Puesto eliminado";
                return RedirectToAction("Puestos");
            }
            catch (Exception ex)
            {
                MensajeError = "Error eliminado";
                return RedirectToAction("Puestos");
            }
        }
    }
}