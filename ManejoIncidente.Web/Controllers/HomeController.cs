﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class HomeController : BaseController
    {
        [VerifySession]
        public ActionResult Index()
        {
            ViewData["GOOD"] = MensajeSucces;
            ViewData["ERROR"] = MensajeError;
            MensajeSucces = null;
            MensajeError = null;
            LiberarMensajes();
            return View();
        }
    }
}