﻿using ManejoIncidente.Logica.Usuarios;
using ManejoIncidente.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ManejoIncidente.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly UsuarioLogica _usuarioLogica;

        public LoginController()
        {
            _usuarioLogica = new UsuarioLogica();
        }
        // GET: Login
        [VerifySession]
        public ActionResult Login()
        {
            //_usuarioLogica.Consultar
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel login)
        {
            try
            {
                if(string.IsNullOrEmpty(login.Clave) || string.IsNullOrEmpty(login.Usuario))
                {
                    ViewData["ERROR"] = "Usuario o Contraseña vacios, favor completar los campos.";
                    return View();
                }
                int idUsuario = _usuarioLogica.Autentica(login.Usuario, login.Clave);
                if (idUsuario.Equals(0))
                {
                    ViewData["ERROR"] = "Usuario o Contraseña Incorrecto";
                    return View();
                }
                var usuario = _usuarioLogica.ConsultarUsuario(idUsuario);
                LoginHelper.NombreUsuario = usuario[0].Nombre;
                LoginHelper.ID = usuario[0].UsuarioId;

                Session["Usuario"] = LoginHelper.NombreUsuario;
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewData["ERROR"] = "Error consultando usuario";
                return View();
            }
        }
    
        public ActionResult Logout()
        {
            Session["Usuario"] = null;
            return RedirectToAction("Login");
        }
    
    }
}