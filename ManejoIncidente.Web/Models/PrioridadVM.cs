﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class PrioridadVM
    {
        public int PrioridadId { get; set; }
        [Display(Name ="Sla")]
        public int SlaId { get; set; }
        public List<Sla> Slas { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }
        public string Sla { get; set; }
    }
}