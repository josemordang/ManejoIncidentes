﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class LoginModel
    {
        public string Usuario { get; set; }
        public string Clave { get; set; }
    }
}