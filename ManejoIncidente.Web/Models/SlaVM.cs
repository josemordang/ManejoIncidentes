﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class SlaVM
    {
        public int SlaId { get; set; }
        [Required(ErrorMessage = "El descripcion  es requerida")]
        [Display(Name ="Descripción")]
        public string Descripcion { get; set; }
        [Display(Name = "Cantida de Horas")]
        public int CantidadHoras { get; set; }
    }
}