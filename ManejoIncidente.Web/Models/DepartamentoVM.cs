﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class DepartamentoVM
    {
        public int DepartamentoId { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }
        //public string Estatus { get; set; }
        //public bool Borrado { get; set; }
        //public DateTimeOffset FechaIngreso { get; set; }
        //public DateTimeOffset FechaModificacion { get; set; }
        //public int CreadoPor { get; set; }
        //public int ModificadoPor { get; set; }
    }
}