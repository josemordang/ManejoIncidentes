﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class PuestoVM
    {
        public int PuestoId { get; set; }
        [Display(Name ="Departamento")]
        public int DepartamentoId { get; set; }
        public List<Departamento> Departamentos { get; set; }
        public string Departamento { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }
    }
}