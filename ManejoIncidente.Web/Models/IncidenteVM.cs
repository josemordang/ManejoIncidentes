﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class IncidenteVM
    {
        public int IncidenteId { get; set; }
        public int UsuarioReportaId { get; set; }

        [Display(Name = "Asignar a")]
        public int UsuarioAsignadoId { get; set; }
        public List<Usuario> Usuarios { get; set; }

        [Display(Name = "Prioridad")]
        public int PrioridadId { get; set; }
        public string PrioridadI { get; set; }
        public List<Prioridad> Prioridades { get; set; }

        [Display(Name = "Departamento")]
        public int DepartamentoId { get; set; }
        public string DepartamentoI { get; set; }
        public List<Departamento> Departamentos { get; set; }
        [Required(ErrorMessage = "El titulo es requerido")]
        [Display(Name = "Titulo")]
        public string Titulo { get; set; }
        [Required(ErrorMessage = "La descripcion es requerida")]
        public string Descripcion { get; set; }
        public DateTimeOffset FechaCierre { get; set; }
        public string ComentarioCierre { get; set; }
        public string Estatus { get; set; }
        public string Usuario { get; set; }
    }
}