﻿using ManejoIncidente.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManejoIncidente.Web.Models
{
    public class UsuarioVM
    {
        public int UsuarioId { get; set; }
        //[Required (ErrorMessage ="El puesto es requerido")]
        public List<Puesto> Puestos { get; set; }
        public int PuestoId { get; set; }
        [Required (ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El apellido es requerido")]
        public string Apellido { get; set; }
        [Required(ErrorMessage = "La cedula es requerida")]
        public string Cedula { get; set; }
        [Required(ErrorMessage = "El correo es requerido")]
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public DateTime FechaNacimiento { get; set; }
        [Required(ErrorMessage = "El nombre de usuario es requerido")]
        public string NombreUsuario { get; set; }
        [Required(ErrorMessage = "La contraseña es requerida")]
        public string Contrasena { get; set; }
        //public string Estatus { get; set; }
        //public DateTimeOffset FechaIngreso { get; set; }
        //public DateTimeOffset FechaModificacion { get; set; }
    }
}