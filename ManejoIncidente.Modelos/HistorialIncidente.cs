﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Modelos
{
    public class HistorialIncidente : Compratido
    {
        public int HistorialId { get; set; }
        public int IncidenteId { get; set; }
        public string Comentario { get; set; }
    }
}
