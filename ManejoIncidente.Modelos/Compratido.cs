﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Modelos
{
    public class Compratido
    {
        public string Estatus { get; set; }
        public bool Borrado { get; set; }
        public DateTimeOffset FechaIngreso { get; set; }
        public DateTimeOffset FechaModificacion { get; set; }
        public int CreadoPor { get; set; }
        public int ModificadoPor { get; set; }
    }
}
