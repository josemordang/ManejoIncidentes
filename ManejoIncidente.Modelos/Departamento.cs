﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Modelos
{
    public class Departamento : Compratido
    {
        public int DepartamentoId { get; set; }
        public string Nombre { get; set; }
    }
}
