﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoIncidente.Modelos
{
    public class Prioridad : Compratido
    {
        public int PrioridadId { get; set; }
        public int SlaId { get; set; }
        public string Nombre { get; set; }
    }
}
